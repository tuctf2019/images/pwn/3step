# 3step -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/3step)

## Chal Info

Desc: `Gonna have to get crafty with this one.`

Given files:

* 3step

Hints:

* Two buffers huh. Can you use both?

Flag: `TUCTF{4nd_4_0n3,_4nd_4_7w0,_4nd_5h3ll_f0r_y0u!}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/3step)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/3step:tuctf2019
```
